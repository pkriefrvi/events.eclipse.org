types:
  - name: "Session"
    id: "session"
    color: "#FF1464"
  - name: "Lightning Talk"
    id: "lightning"
    color: "#F5911D"
  - name: "Panel"
    id: "panel"
    color: "#86507E"
  - name: "Break"
    id: "break"
    color: "#000000"
items:
  - name: "Summit Introduction"
    type: "session"
    time: "13:00 - 13:10 CEST"
  - name: "Navigating the Java Seas: Jenkins' Journey with Temurin"
    type: "session"
    time: "13:10 - 13:50 CEST"
    presenters:
      - "Bruno Verachten"
    abstract: |
      <p>
        Ahoy, fellow voyagers! Join us aboard the Jenkins ship as we embark on an
        exhilarating journey with Temurin.
      </p>
      <p>
        Set sail with us as we navigate through the seas of JDK management:
        From the choppy waters of Docker images to the smooth sailing of
        Temurin binaries.
      </p>
      <p>
        Discover how we've optimized performance, enhanced CVE response times,
        and explored uncharted territories like testing JDK21 on RISC-V.
      </p>
      <p>
        But that’s not all! Learn about our efforts to improve Adoptium API
        integration and our vision for the future of JDK management in Jenkins.
      </p>
      <p>
        Prepare to be inspired and energized as we chart our course towards
        smoother seas and brighter horizons.
      </p>
      <p>
        Don’t miss this opportunity to join our crew and be part of the
        adventure!
      </p>
  - name: "Java-riety is the Spice of Life"
    type: "lightning"
    time: "13:50 - 14:10 CEST"
    presenters:
      - "Haroon Khel"
    abstract: |
      <p>
        From the outside the release process at Adoptium may look overwhelming; 5
        different java versions (at the time of writing) each released on
        multiple platforms. This talk will explore what it takes from an
        infrastructure perspective to facilitate a smooth and stable release
        process. The talk will provide an overview of the builds and tests we run
        at Adoptium before announcing a successful release, as well as a
        comparison with other popular Java vendors.
      </p>
  - name: "Break"
    type: "break"
    time: "14:10 - 14:15 CEST"
  - name: "Modernize Java for Cloud: Lessons Learned from Using EMT4J to Upgrade Applications for Alibaba Scale"
    type: "session"
    time: "14:15 - 14:55 CEST"
    presenters:
      - San Hong Li
    abstract: |
      <p>
        We (Alibaba), as one of the largest Java users in the world, have been
        on a perpetual journey of upgrading Java versions in our production
        systems. In this talk, we will share our engaging experiences with
        EMT4J and the lessons we have learned from using EMT4J to migrate
        extremely distributed Java applications running on massive
        infrastructure. We will describe the technical challenges we faced when
        upgrading our applications in the cloud and the solutions with EMT4J we
        are using. While your workloads are different, the thought practices we
        went through could be helpful for you.
      </p>
      <p>
        You'll learn about: 
      </p>
      <ol>
        <li>What’s the EMT4J, and how can you use it for
          your upgrade?
        </li>
        <li>
          The scale of Alibaba Cloud’s internal Java
          ecosystem, how we manage the migrating efforts 
        </li>
        <li>
          The upgrading process/practices we used in our production system and
          the lessons we have learned.
        </li>
      </ol>
  - name: "What's Lurking in Your JDK? Identifying Vulnerabilities in Temurin"
    type: "lightning"
    time: "14:55 - 15:15 CEST"
    presenters:
      - "Daniel Scanteianu"
    abstract: |
      <p>
        A Vulnerability Disclosure Report (VDR) is to software what a nutrition
        label is to food. In this talk, we’ll review the Temurin VDR, what goes
        in it, how to interpret it, and how to determine what your next steps
        should be if the version you are using is vulnerable. We’ll discuss
        what Common Vulnerabilities and Exposures (CVEs) are and how they are
        reported. We’ll point out where we get our information from (e.g., the
        OpenJDK Vulnerability Group and NIST Database). We’ll also discuss the
        CycloneDX format we use for reporting and will walk through a sample
        VDR.
      </p>
  - name: "Building the World's Most Secure OpenJDK Distribution"
    type: "session"
    time: "15:15 - 15:55 CEST"
    presenters:
      - "Michael Winser"
      - "Shelley Lambert"
    abstract: |
      <p>
        Software supply chain security is an industry-wide problem that impacts
        both open source and proprietary software. With attack vectors present
        at every stage of the software development process and thousands of
        dependencies, this is a technical debt that demands innovative
        solutions, scalable processes, and years of effort to address. The
        Eclipse Foundation and the Adoptium Project are leading the way with
        Eclipse Temurin and a common security infrastructure to make open
        source software more secure. The presentation will explain how and why
        the Adoptium Project and the Eclipse Foundation are combining forces to
        make Temurin the world’s most secure OpenJDK Distribution and why
        anyone running Java applications should care.
      </p>
  - name: "Break"
    type: "break"
    time: "15:55 - 16:00 CEST"
  - name: "Inspiring AI-Infused Innovations in Adoptium AQAvit and Openj9"
    type: "lightning"
    time: "16:00 - 16:20 CEST"
    presenters:
      - Lan Xia
      - Longyu Zhang (Co-Presenter)
    abstract: |
      <p>
        In today’s fast-paced technological landscape, maintaining efficiency,
        quality, and reliability in projects is critical for any project
        development. Embark on a journey of inspiration as we explore the
        fusion of AI with Adoptium AQAvit and Openj9. In this talk, we’ll delve
        into visionary AI concepts and initiatives within both projects,
        illuminating pathways for transformative change. From dynamic issue
        triage and seamless assignment to proactive issue creation, test
        prioritization, and foresighted issue prediction, we’ll unveil the
        boundless possibilities that AI integration offers. Moreover, we will
        explore the tangible benefits and pragmatic implications of infusing AI
        into AQAvit testing and Openj9 workflows, including accelerated testing
        and release cycles, heightened fault detection capabilities, and
        diminished manual workload. Through real-world case studies, attendees
        will gain insights into how AI can revolutionize testing and triaging,
        paving the way for more robust and reliable software development
        practices.
      </p>
  - name: "Validating Infrastructure Security Through Access Auditing"
    type: "lightning"
    presenters:
      - Scott Fryer
    time: "16:20 - 16:40 CEST"
    abstract: |
      <p>
       The Adoptium project utilises infrastructure from a variety of providers, 
       and due to the publicly open nature of the project, ensuring the supply chain 
       and build infrastructure machines are secure is a key objective.
      </p>
      <p>
        This talk will discuss implementing tools (most notably Wazuh) to extend the default auditing available on systems) 
        to more easily enable access audits, provide visibility of the volume of attacks, 
        and allow a great degree of confidence in our implemented security policies. 
        This 10-minute lightning talk will cover the importance of proving that implemented security measures are effective. 
      </p>
  - name: "Adoptium 2024 Review and plan"
    type: "lightning"
    presenters:
      - Carmen Delgado
    time: "16:40 - 16:55 CEST"
    abstract: |
      <p>
        This is an opportunity to celebrate all the achievements done by the Adoptium community based on 
        their annual plan and what is still waiting to be done with the community contribution.
      </p>
