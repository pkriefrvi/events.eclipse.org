---
title: "Speakers"
date: 2023-10-12T12:00:00-04:00
description: ""
categories: []
keywords: []
slug: ""
aliases: []
toc: false
hide_page_title: false
hide_sidebar: true
hide_breadcrumb: false
layout: "speakers"
#header_wrapper_class: ""
#seo_title: ""
#headline: ""
#subtitle: ""
#tagline: ""
#links: []
---

{{< events/user_bios event="theiacon" year="2023" source="speakers" imgRoot="/events/2023/theiacon/images/speakers/" >}}
