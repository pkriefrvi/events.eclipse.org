---
title: "Program Committee"
seo_title: "Program Committee | Theiacon 2024"
date: 2024-08-07
description: "The program committee for Theiacon 2024"
categories: []
keywords: ["theiacon", "2024", "program committee"]
hide_page_title: false
hide_sidebar: true
hide_breadcrumb: false
---

{{< events/user_bios event="theiacon" year="2024" source="committee" imgRoot="/events/2024/theiacon/images/committee/" >}}
